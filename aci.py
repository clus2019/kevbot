import requests
import os
import re
#from conversation import Conversation


APIC_URL = os.environ.get('APIC_URL', None)
APIC_USERNAME = os.environ.get('APIC_USERNAME', None)
APIC_PASSWORD = os.environ.get('APIC_PASSWORD', None)

session = requests.Session()
session.verify = False

def _apic_login():
    login_resp = session.post(APIC_URL+"/api/aaaLogin.json", json={'aaaUser': {'attributes': {'name':APIC_USERNAME, 'pwd':APIC_PASSWORD}}})

def apic_api_call(method, endpoint, json=None):
    resp = session.request(method, APIC_URL+endpoint, json=json)
    if resp.status_code==403:
        _apic_login()
        
        resp = session.request(method, APIC_URL+endpoint, json=json)
    
    return resp
    
def resp_ok(resp):
    return resp.status_code >= 200 and resp.status_code < 300
    
def cmd_show_interface(args):
    "Show EPGs configured on  the interface"
    node = args['node']
    interface = args['interface']
    query = "/api/mo/uni.json?query-target=subtree&target-subtree-class=fvRsPathAtt&query-target-filter=eq(fvRsPathAtt.tDn,\"topology/pod-1/paths-%s/pathep-[%s]\")" % (node, interface)
    
    res = apic_api_call('GET', query).json()
    
    encaps=[]
    for path in res['imdata']:
        attrs = path['fvRsPathAtt']['attributes']
        m = re.match(r'(.*)/rspathAtt', attrs['dn'])
        epg_dn = m.group(1)
        encap = attrs['encap']
        encaps.append("%s: %s" % (encap, epg_dn))
    
    reply = "Encaps on leaf %s interface %s: \n\n%s" % (node, interface, '\n'.join(encaps))
    
    return reply
    
def cmd_fabric_health(args):
    "Show the fabric health score"
    query = '/api/node/mo/topology/health.json'
    res = apic_api_call('GET', query).json()
    
    try:
        health = res['imdata'][0]['fabricHealthTotal']['attributes']['cur']
    except:
        health = 'unknown'
        
    return "Current fabric health score is %s %%"%health

def cmd_tenant_list(args):
    "Show list of Tenants"
    query = '/api/node/class/fvTenant.json'
    res = apic_api_call('GET', query).json()
    text_tenant_list = ''
    try:
        tenant_list = res['imdata']
    except:
        tenant_list = 'unknown'
    for t in tenant_list:
        text_tenant_list += t['fvTenant']['attributes']['name'] + '\n'    
    return text_tenant_list
  
def _create_option_list(query, class_name, attribute_name):
    res = apic_api_call('GET', query).json()
    result_list = []
    try:
        for result in res['imdata']:
            result_list.append(result[class_name]['attributes'][attribute_name])
            
        return result_list
    except:
        return None
        
def _extract_interface(interface_name):
    """
        Takes an interface name (e.g. eth101/1/20 or e1/2) and returns a dict with 'fex', 'mod' and 'int' keys. 
    """
    return re.match(r'e(th?)?((?P<fex>\d+)/)?(?P<mod>\d+)/(?P<int>\d+)', interface_name).groupdict()
    
def _extract_vlan(vlan):
    """
        e.g. Takes vlan='vlan-28', returns 28 or None if invalid
    """
    try:
        return re.match(r'vlan-(\d+)', vlan).group(1)
    except:
        return None
        
def _extract_leaf(leaf):
    """
        e.g. Takes leaf='leaf-121', returns 121 or None if invalid
    """
    try:
        return re.match(r'leaf-(\d+)', leaf).group(1)
    except:
        return None

