#!/usr/bin/env python
#  -*- coding: utf-8 -*-
"""A simple bot script, built on Flask.

This sample script leverages the Flask web service micro-framework
(see http://flask.pocoo.org/).  By default the web server will be reachable at
port 5000 you can change this default if desired (see `flask_app.run(...)`).

ngrok (https://ngrok.com/) can be used to tunnel traffic back to your server
if your machine sits behind a firewall.

You must create a Webex Teams webhook that points to the URL where this script
is hosted.  You can do this via the WebexTeamsAPI.webhooks.create() method.

Additional Webex Teams webhook details can be found here:
https://developer.webex.com/webhooks-explained.html

A bot must be created and pointed to this server in the My Apps section of
https://developer.webex.com.  The bot's Access Token should be added as a
'WEBEX_TEAMS_ACCESS_TOKEN' environment variable on the web server hosting this
script.

This script supports Python versions 2 and 3.

Copyright (c) 2016-2019 Cisco and/or its affiliates.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


# Use future for Python v2 and v3 compatibility
from __future__ import (
    absolute_import,
    division,
    print_function,
    unicode_literals,
)
from builtins import *


__author__ = "Chris Lunsford"
__author_email__ = "chrlunsf@cisco.com"
__contributors__ = ["Brad Bester <brbester@cisco.com>"]
__copyright__ = "Copyright (c) 2016-2019 Cisco and/or its affiliates."
__license__ = "MIT"

from ngrokwebhook import get_ngrok_public_url, delete_webhooks_with_name, create_ngrok_webhook
import aci
from flask import Flask, request
import requests
import json
import os
import re

from webexteamssdk import WebexTeamsAPI, Webhook


# Module constants
CAT_FACTS_URL = 'https://catfact.ninja/fact'
WEBHOOK_NAME = "ngrok_webhook"
dummy = 0

# Initialize the environment
# Create the web application instance
flask_app = Flask(__name__)
# Create the Webex Teams API connection object
api = WebexTeamsAPI()

def cmd_hello(args):
    "Say hello"
    return "yeah?"
    
def cmd_error(args):
    return "Sorry, an error occurred: %s" % str(args['exception']), args['from_email'], args['room_id']

def cmd_help(args):
    "Show the help message"
    help_str = generate_help_string('', None, COMMANDS)
    return help_str

def generate_help_string(cur, parent, cmd_tree_ptr):
    print ("generate_help_string called with cur %s" % cur)
    new_cur = cur
    
    for cmd_token, subtree in cmd_tree_ptr:
        print ("Processing token %s", cmd_token)
        res = re.match(r'\(\?P(<.*>)', cmd_token)
        if res:
            cmd_token = res.group(1)
        
        if isinstance(subtree, list):
            if parent:
                new_parent = parent + ' ' + cmd_token
            else:
                new_parent = cmd_token
            new_cur = new_cur + generate_help_string(cur, new_parent, subtree)
        else:
            if parent:
                new_cur = new_cur + parent + ' ' + cmd_token + ': '
            else:
                new_cur = new_cur + cmd_token + ': '
                
            try:
                new_cur = new_cur + subtree.__doc__ + '\n'
            except:
                new_cur = new_cur + '<no help available>\n'
                
    return new_cur

# Helper functions
def get_catfact():
    """Get a cat fact from catfact.ninja and return it as a string.

    Functions for Soundhound, Google, IBM Watson, or other APIs can be added
    to create the desired functionality into this bot.

    """
    response = requests.get(CAT_FACTS_URL, verify=False)
    response.raise_for_status()
    json_data = response.json()
    return json_data['fact']


# Core bot functionality
# Your Webex Teams webhook should point to http://<serverip>:5000/events
@flask_app.route('/events', methods=['GET', 'POST'])
def webex_teams_webhook_events():
    """Processes incoming requests to the '/events' URI."""
    if request.method == 'GET':
        return ("""<!DOCTYPE html>
                   <html lang="en">
                       <head>
                           <meta charset="UTF-8">
                           <title>Webex Teams Bot served via Flask</title>
                       </head>
                   <body>
                   <p>
                   <strong>Your Flask web server is up and running!</strong>
                   </p>
                   <p>
                   Here is a nice Cat Fact for you:
                   </p>
                   <blockquote>{}</blockquote>
                   </body>
                   </html>
                """.format(get_catfact()))
    elif request.method == 'POST':
        """Respond to inbound webhook JSON HTTP POST from Webex Teams."""

        # Get the POST data sent from Webex Teams
        json_data = request.json
        print("\n")
        print("WEBHOOK POST RECEIVED:")
        print(json_data)
        print("\n")

        # Create a Webhook object from the JSON data
        webhook_obj = Webhook(json_data)
        # Get the room details
        room = api.rooms.get(webhook_obj.data.roomId)
        # Get the message details
        message = api.messages.get(webhook_obj.data.id)
        # Get the sender's details
        person = api.people.get(message.personId)

        print("NEW MESSAGE IN ROOM '{}'".format(room.title))
        print("FROM '{}'".format(person.displayName))
        print("MESSAGE '{}'\n".format(message.text))

        # This is a VERY IMPORTANT loop prevention control step.
        # If you respond to all messages...  You will respond to the messages
        # that the bot posts and thereby create a loop condition.
        me = api.people.me()
        if message.personId == me.id:
            # Message was sent by me (bot); do not respond.
            return 'OK'

        else:
            # Message was sent by someone else; parse message and respond.
            if "/CAT" in message.text:
                print("FOUND '/CAT'")
                # Get a cat fact
                cat_fact = get_catfact()
                print("SENDING CAT FACT '{}'".format(cat_fact))
                # Post the fact to the room where the request was received
                api.messages.create(room.id, text=cat_fact)
            if "show" and "ACI" and "health" in message.text:
                api.messages.create(room.id, text=aci.cmd_fabric_health(dummy))
            else:
                func, args = find_cmd_func_and_args(message.text)
                api.messages.create(room.id, text=func(args))
            return 'OK'

COMMANDS = [
    ("help", cmd_help),
    ("hello", cmd_hello),
    ("show", [
        ("health", aci.cmd_fabric_health),
        ("tenants", aci.cmd_tenant_list),
        ("leaf", [
            (r'(?P<node>\d+)', [
                ('interface', [
                    (r'(?P<interface>et?h?\d+/\d+)', aci.cmd_show_interface)
                ]),
            ])
        ]),
    ]),
    # ("set", [
    #     ("vlan", [
    #         (r'(?P<vlan>\d+)', [
    #             ('leaf', [
    #                 (r'(?P<leaf>\d+)', [
    #                     'interface', [
    #                         (r'(?P<interface>et?h?\d+/\d+)', [
    #                             ('tenant', [(r'(?P<tenant>\w+)')])
    #                         ])
    #                     ]
    #                 ])
    #             ])
    #         ])
    #     ]
    #     )
    # ]),
    #("configure", [
    #    ("vlan", [
    #        ("on", [
    #            ("port", aci.ConfigureVlanPort)
    #        ])
    #    ])
    #]),
    #("interface", [
    #    ("description", aci.InterfaceDescription)
    #]),
    #("create", [
    #    ("vlan", [
    #        (r'(?P<vlan>\d+)', nxos.cmd_create_vlan)
    #    ])
    #]),
    #("get", [
    #    ("vlans", nxos.cmd_get_vlans)
    #]),
    #("launch", [
    #    ("vm", cloud_center.CloudCenterLaunchVM)
    #])
]
def find_cmd_func_and_args(message):
    tokens = message.split()
    cmd_tree_ptr = COMMANDS
    collected_args={}
    
    while len(tokens)>0:
        tok = tokens.pop(0)
        matched_tokens=[]
        for cmd_token, func_or_subcmd in cmd_tree_ptr:
            if cmd_token[0]=='(':
                # This is a regex that should match an arg
                m = re.match(cmd_token, tok)
                if m:
                    collected_args.update(m.groupdict())
                    matched_tokens.append((cmd_token, func_or_subcmd))
            # TODO - need to sanitise token before using it as RE?
            elif re.match(tok, cmd_token):
                print ("matched token '%s'"%cmd_token)
                matched_tokens.append((cmd_token,func_or_subcmd))
                
        if len(matched_tokens)>1:
            print ("multiple matches")
            break
        
        if len(matched_tokens)==1:
            func_or_subcmd = matched_tokens[0][1]
            if isinstance(func_or_subcmd, list):
                cmd_tree_ptr = func_or_subcmd
                continue
            return (func_or_subcmd, collected_args)
                
    # Didn't find a command - return help
    return (cmd_help, {})

if __name__ == '__main__':
    # Start the Flask web server
    kevpublic_url = get_ngrok_public_url()
    delete_webhooks_with_name(api, name=WEBHOOK_NAME)    
    kevwebhook = create_ngrok_webhook(api, kevpublic_url)    
    try:
        print ("starting flask.  Hit any key to stop")
        flask_app.run(host='0.0.0.0', port=5000)
    except KeyboardInterrupt:
        print ("Interrupt received, stopping")    
    finally:
        print("stopped flask")
