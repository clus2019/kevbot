import os
from acitoolkit.acitoolkit import *
from webexteamssdk import WebexTeamsAPI

APIC_URL = os.environ.get('APIC_URL', None)
APIC_USERNAME = os.environ.get('APIC_USERNAME', None)
APIC_PASSWORD = os.environ.get('APIC_PASSWORD', None)
ROOM_ID = "Y2lzY29zcGFyazovL3VzL1JPT00vOTE0ZWMzYTAtN2JlYi0xMWU5LTkxOTktZGIxMTdhNjBmMjFm"

api = WebexTeamsAPI()

def main():
    session = Session(APIC_URL, APIC_USERNAME, APIC_PASSWORD)
    session.login()

    subscribe_to_events(session)


def subscribe_to_events(session):
    Tenant.subscribe(session, only_new=True)
    AppProfile.subscribe(session, only_new=True)
    EPG.subscribe(session, only_new=True)

    while True:
        if Tenant.has_events(session):
            event = Tenant.get_event(session)
            if event.is_deleted():
                status = "has been deleted"
            else:
                status = "has been created/modified"
            tenant_name = event.dn.replace("uni/tn-", '')
            post_message_to_spark("{} tenant {}".format(tenant_name, status))

        elif AppProfile.has_events(session):
            event = AppProfile.get_event(session)
            if event.is_deleted():
                status = "has been deleted"
            else:
                status = "has been created/modified"

            post_message_to_spark("{} {}".format(event.dn, status))

        elif EPG.has_events(session):
            event = EPG.get_event(session)
            if event.is_deleted():
                status = "has been deleted"
            else:
                status = "has been created/modified"

            post_message_to_spark("{} {}".format(event.dn, status))


def post_message_to_spark(message):
    message = api.messages.create(ROOM_ID, text=message)
    print message

if __name__ == "__main__":
    main()
